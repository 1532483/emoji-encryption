var s = {}

s.init = function(){
  s.next1 = U.$("next1");
  s.next2 = U.$("next2");
  s.next3 = U.$("next3");
  s.slides = [U.$("slide1"), U.$("slide2"), U.$("slide3")];
  document.cookie = "visits = 1";

  U.addEvent(s.next1, "click", gotoSlide);
  U.addEvent(s.next2, "click", gotoSlide);
  U.addEvent(s.next3, "click", gotoSlide);
}

function gotoSlide(){
  for(var i = 0; i<s.slides.length; i++){
    if(i === s.slides.length - 1){
      window.location.replace("/html/encrypt.html");
      return;
    }
    if(!s.slides[i].hidden){
      s.slides[i].hidden = true;
      s.slides[i+1].hidden = false;
      return;
    }
  }
}


U.addEvent(document, "DOMContentLoaded", s.init);
