"use script";
var en = {};

/**
 * Helper method to get an array of string from
 * a range of characters code
 * @param {char} charStart - first character in the array
 * @param {char} charEnd - last character in the array
 * @return {array} array of characters
*/
function charRangeArray(charStart, charEnd){
  var arr = [];
  for(var i = charStart.charCodeAt(0); i <= charEnd.charCodeAt(0); i++){
    arr.push(String.fromCharCode(i));
  }
  return arr;
}

/**
 * Generates a grid filled with an array content
 * @param {array} arr - array to fill the table
*/
function generateGridOption(arr){
  var index = 1;
  for(var i=0; i<arr.length; i++){
    var p = document.createElement("p");
    p.id = index;
    index++;
    if(typeof arr[i] === "string"){
      p.innerHTML = arr[i];
      p.className = "letter";
    }else{
      p.innerHTML = twemoji.parse(String.fromCodePoint(arr[i]));
      p.className = "containEmoji";
    }
    en.keySelect.appendChild(p);
  }
}

/**
 * Checks if the browser is old by validating
 * that .addEventListener is undefined
 * @return {boolean} - boolean of whether or not it is old
*/
function isOldBrowser(){
  if(window.addEventListener !== undefined && document.textContent !== undefined){
    return false;
  }else{
    return true;
  }
}

/**
 * Sets emoji inside an array and the weather array
 * while generating the grid and adding the input event
 * @param {XMLHttpRequest} request - response of request
*/
function setEmoji(request){
    var emojis = JSON.parse(request.responseText);
    var arr = [];
    en.weatherArr = [];
    var weather = ["clouds", "clear", "snow", "thunderstorm","rain","drizzle", "extreme", "atmosphere"];
    for(var i=0; i<en.ASCIIChars.length; i++){
      arr.push(emojis[i]["codePoint"]);
    }
    for(var i=0; i<weather.length; i++){
      en.weatherArr.push({ "emoji": emojis[i]["codePoint"], "name": weather[i]});
    }
    en.emoji = arr;
    generateGridOption(en.emoji);
    U.addEvent(en.keySelect, "click", addEventToText);
}

/**
 * Helper method to handle an http request
 * @param {string} url - path to the server request
 * @param {function} callback - callback function to handle when the
 * request is ready
 */
function callHttpRequest(url, callback){
  var request = new XMLHttpRequest();
  request.open("GET", url, true);
  request.send(null);
  request.onreadystatechange = function(){
    if(request.readyState === 4){
      if(request.status >= 200 && request.status < 300 || request.status == 304){
        callback(request);
      }else{
        console.log("The request failed");
      }
    }
  }
}

/**
 * Sets encryption layout according to the browser's version
*/
function setEncryptionMode(){
  if(isOldBrowser()){
    generateGridOption(en.alphabetLower.concat(en.numeric));
    U.addEvent(en.keySelect, "click", addEventToText);
  }else{
    callHttpRequest("/JSON/emojiList.json", setEmoji);
    createWeatherSelector();
  }
}

/**
 * Sets layout and add elements for the weather input
*/
function createWeatherSelector(){
  var button = document.createElement("button");
  button.type = "button";
  button.innerHTML = "Encrypt By Weather";
  button.style.marginLeft = "10px";
  button.style.marginRight = "10px";
  button.style.width = "180px";
  button.style.height = "30px";
  en.inWeather = document.createElement("input");
  en.inWeather.type = "text";
  en.inWeather.placeholder = "Enter city";
  en.inWeather.value = getCityCookie();
  U.$("weather").appendChild(button);
  U.$("weather").appendChild(en.inWeather);
  U.addEvent(button, "click", checkWeather);
}

/**
 * Returns a string of the last city entered and stored in a cookie
 * @return {string} - string of city
*/
function getCityCookie(){
  var cookies = document.cookie.split(";");
  for(var i = 0; i < cookies.length; i++){
    var cookie = cookies[i];
    if(cookie.split("=")[0].trim() === "city"){
      return cookie.split("=")[1].trim();
    }
  }
  return "";
}

/**
 * Event handler triggered when button "Encrypt By Weather"
 * Gets input text and send request to API
*/
var anteriorCity;
function checkWeather(){
  if(en.inWeather.value !== "" && anteriorCity !== en.inWeather.value){
    anteriorCity = en.inWeather.value;
    var url = "http://api.openweathermap.org/data/2.5/weather?q="+encodeURIComponent(en.inWeather.value)+"&appid=b53c63b6ec413179f0c818cfc79daea9";
    callHttpRequest(url, handleWeather);
  }
}

/**
 * Gets the weather request and display in displayKey
 * @param {XMLHttpRequest} request
*/
function handleWeather(request){
  var obj = JSON.parse(request.responseText);
  for(var i=0; i<en.weatherArr.length; i++){
    if(en.weatherArr[i]["name"] === obj["weather"][0]["main"].toLowerCase()){
      en.key = (en.weatherArr[i]["emoji"]);
      displayKey(String.fromCodePoint(en.key));
      en.inputText.placeholder = "Enter a message";
      en.inputText.disabled = false;
      U.addEvent(en.inputText, "keyup", encryptMessageEmoji);
      break;
    }
  }
  document.cookie = "city=" + en.inWeather.value;
}

/**
 * Gets the input text and handles the display
*/
function encryptMessageOld(){
  var message = en.inputText.value;
  if(!en.englishRegex.test(message)){
    console.log("Not a valid input stop it");
    return;
  }
  en.displayResult.textContent = shift(message, en.ASCIIChars.indexOf(en.key), en.ASCIIChars);
}

/**
 * Gets the input text and handles the display with emojis
*/
function encryptMessageEmoji(){
  var message = en.inputText.value;
  if(!en.englishRegex.test(message)){
    console.log("Not a valid input");
    return;
  }
  message = mergeEmojis(parseEmoji(message));
  if(message.length > Math.floor(en.displayResult.clientWidth / 48) * Math.floor(en.displayResult.clientHeight / 48)){
    console.log("limit reached");
    return;
  }
  en.displayResult.innerHTML = twemoji.parse(shift(message, en.emoji.indexOf((typeof en.key === "number") ? en.key : en.key.codePointAt(0)) + 1, en.emoji));
}

/**
 * Converts ASCII string to emoji equivalent
 * @param {string} message - plain text
*/
function parseEmoji(message){
  var textEmoji = "";
  for(var i=0; i<message.length; i++){
    var char = message[i];
    var index = en.ASCIIChars.indexOf(char);
    if(index !== -1){
      textEmoji += String.fromCodePoint(en.emoji[index]);
    }else{
      textEmoji += char;
    }
  }
  return textEmoji;
}

/**
 * Encrypts the message using ceasar cipher
 * @param {string} message
 * @param {number} key - number to shift
 * @param {array} arr - reference to characters
 * @return text - cipher text
*/
function shift(message, key, arr){
  var text = "";
  for(var i = 0; i < message.length; i++){
    var char = message[i];
    if(isOldBrowser()){
      var pos = arr.indexOf(char);
      if(pos !== -1){
        var newPos = (pos + key) % arr.length;
        text += arr[newPos];
      }else{
        text += char;
      }
    }else{
      var pos = arr.indexOf(char.codePointAt(0));
      if(pos !== -1){
        var newPos = (pos + key) % arr.length;
        text += String.fromCodePoint(arr[newPos]);
      }else{
        text += char;
      }
    }
  }
  return text;
}

/**
 * Creates and returns an array of emoji characters
 * if they are emojis
 * @param {string} message
 * @return arr
*/
function mergeEmojis(message){
  var arr = [];
  for(var i=0; i<message.length; i++){
    if(message.codePointAt(i) > 65535){
      arr.push(message[i] + message[i+1]);
      i += 1;
    }else{
      arr.push(message[i]);
    }
  }
  return arr;
}

/**
 * Display key in a div
 * @param {string} key
*/
function displayKey(key){
  var selectedKey = document.getElementById("selectedKey");
  while(selectedKey.firstChild){
    selectedKey.removeChild(selectedKey.firstChild);
  }
  var keyDisplayed = (en.ASCIIChars.indexOf(key) !== -1) ? key : twemoji.parse(key);
  var p = document.createElement("p");
  p.innerHTML = keyDisplayed;
  var element = p.getElementsByClassName("emoji")[0];
  if(element){
    element.className = "emojiDisplayed";
  }
  p.style.fontSize = "60px";
  selectedKey.appendChild(p);
}

/**
 * Enables input on message and attach necessary event hanlder to events
 * @param {event} e - event argument
*/
var firstCall = true;
function addEventToText(e){
  e = e || window.event;
  var target = e.target || e.srcElement;
  if(firstCall){
    if(isOldBrowser()){
      en.key = target.textContent || target.innerText;
      if(en.key.length === 1){
        displayKey(en.key);
        U.addEvent(en.inputText, "keyup", encryptMessageOld);
        en.inputText.focus();
      }else{
        en.key = undefined;
      }
    }else{
      en.key = target.alt;
      if(en.key){
        displayKey(en.key);
        U.addEvent(en.inputText, "keyup", encryptMessageEmoji);
        en.inputText.focus();
      }
    }
    if(en.key){
      en.inputText.placeholder = "Enter a message";
      en.inputText.disabled = false;
      firstCall = false;
    }
  }else{
    if(isOldBrowser()){
      en.key = target.textContent || target.innerText;
      if(en.key.length === 1){
        displayKey(en.key);
        encryptMessageOld();
      }
    }else{
      en.key = target.alt;
      if(en.key){
        displayKey(en.key);
        encryptMessageEmoji();
      }
    }
  }
}

/**
 * Initializes global variables and assign event handlers
*/
en.init = function(){
  //array [a-z]
  en.alphabetLower = charRangeArray("a","z");
  //array[A-Z]
  en.alphabetUpper = charRangeArray("A", "Z");
  //array[0-9]
  en.numeric = charRangeArray("0", "9");
  en.ASCIIChars = en.alphabetLower.concat(en.alphabetUpper).concat(en.numeric).concat(["?", "&", "!", " ", "(", ")", ".", "%", "$", "/"]);
  //regex for english characters
  en.englishRegex = /^[A-Za-z0-9?!& ().%$/]*$/;
  en.keySelect = document.getElementById("keySelect");
  en.inputText = document.getElementById("clearText");
  en.displayResult = document.getElementById("resultEncrypt");
  setEncryptionMode();
}

U.addEvent(document, "DOMContentLoaded", en.init);
