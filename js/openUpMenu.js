var m = {};
'use strict'

/**
 * JavaScript file used to deal with the open up menu
 * and check the first visit to send the wizard
*/
U.addEvent(document, "DOMContentLoaded", function(){
  m.openMenu = document.getElementById("openMenu");
  m.closeMenu = document.getElementById("closeMenu");
  m.navbar = document.getElementById("navbar");
  U.addEvent(m.openMenu, "click", function(){
    m.navbar.style.width = "250px";
  });
  U.addEvent(m.closeMenu, "click", function(){
    m.navbar.style.width = "0";
  });
  checkFirstVisit();
});


/**
 * Checks for first visit. If it is the first visit, then the cookie is empty.
*/
function checkFirstVisit(){
  var cookie = document.cookie;
  if(cookie === ""){
      showWizard();
  }
}

function showWizard(){
  document.cookie = "visits=1";
  window.location.replace("/html/wizardWindow.html");
}
