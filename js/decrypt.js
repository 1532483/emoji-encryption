"use script";
var en = {};

/**
 * Helper method to get an array of string from
 * a range of characters code
 * @param {char} charStart - first character in the array
 * @param {char} charEnd - last character in the array
 * @return {array} array of characters
*/
function charRangeArray(charStart, charEnd){
  var arr = [];
  for(var i = charStart.charCodeAt(0); i <= charEnd.charCodeAt(0); i++){
    arr.push(String.fromCharCode(i));
  }
  return arr;
}

/**
 * generates a table filled with an array
 * and generates the number of rows & cols necessary
 * @param {array} arr - array to fill the table
*/
function generateGridOption(arr){
    var index = 1;
    for(var i=0; i<arr.length; i++){
      var p = document.createElement("p");
      p.id = index;
      index++;
      if(typeof arr[i] === "string"){
        p.innerHTML = arr[i];
        p.className = "letter";
      }else{
        p.className = "containEmoji";
        p.innerHTML = twemoji.parse(String.fromCodePoint(arr[i]));
      }
      en.keySelect.appendChild(p);
    }
}

/**
 * Checks if the browser is old by validating
 * that .addEventListener is undefined
 * @return {boolean} - boolean of whether or not it is old
*/
function isOldBrowser(){
  if(window.addEventListener !== undefined && document.textContent !== undefined){
    return false;
  }else{
    return true;
  }
}

/**
 * sets emoji arrays of the grid
 * while generating the grid and adding the input event
 * @param {XMLHttpRequest} request - response of request
*/
function setEmoji(request){
    var emojis = JSON.parse(request.responseText);
    var arr = [];
    for(var i=0; i<en.ASCIIChars.length; i++){
      arr.push(emojis[i]["codePoint"]);
    }
    en.emoji = arr;
    generateGridOption(en.emoji);
    U.addEvent(en.keySelect, "click", addEventToText);
}

/**
 * Helper method to handle an http request
 * @param {string} url - path to the server request
 * @param {function} callback - callback function to handle when the
 * request is ready
 */
function callHttpRequest(url, callback){
  var request = new XMLHttpRequest();
  request.open("GET", url, true);
  request.send(null);
  request.onreadystatechange = function(){
    if(request.readyState === 4){
      if(request.status >= 200 && request.status < 300 || request.status == 304){
        callback(request);
      }else{
        console.log("The request failed");
      }
    }
  }
}

/**
 * Sets decryption layout according to the browser's version
*/
function setDecryptionMode(){
  if(isOldBrowser()){
    generateGridOption(en.alphabetLower.concat(en.numeric));
    U.addEvent(en.keySelect, "click", addEventToText);
  }else{
    callHttpRequest("/JSON/emojiList.json", setEmoji);
  }
}

/**
 * Gets the input text and handles the display
*/
function decryptMessageOld(){
    var message = en.inputText.value;
    if(!en.englishRegex.test(message) && message.length !== 0){
      console.log("Not a valid input stop it");
      return;
    }
    en.displayResult.textContent = shift(message, en.ASCIIChars.indexOf(en.key), en.ASCIIChars);
}

/**
 * Gets the input text and handles the display with emojis
*/
function decryptMessageEmoji(){
  var message = en.inputText.value;
  for(var i=0; i<message.length; i++){
    if(en.englishRegex.test(message[i])){
      console.log("Not a valid input");
      return;
    }
  }
  message = parseASCII(message);
  if(message.length > 245){
    console.log("limit reached");
    return;
  }
  en.displayResult.innerHTML = twemoji.parse(shift(message, en.emoji.indexOf(en.key.codePointAt(0)) + 1, en.ASCIIChars));
}

/**
 * Converts emoji to ASCII string equivalent
 * @param {string} message - plain text
*/
function parseASCII(message){
  var textASCII = "";
  message = mergeEmojis(message);
  for(var i=0; i<message.length; i++){
    var char = message[i];
    var index = en.emoji.indexOf(char.codePointAt(0));
    if(index !== -1){
      textASCII += en.ASCIIChars[index];
    }else{
      textASCII += char;
    }
  }
  return textASCII;
}

/**
 * Decrypts the message using ceasar cipher
 * @param {string} message
 * @param {number} key - number to shift
 * @param {array} arr - reference to characters
 * @return text - cipher text
*/
function shift(message, key, arr){
  var text = "";
  for(var i = 0; i < message.length; i++){
    var char = message[i];
      var pos = arr.indexOf(char);
      if(pos !== -1){
        if(pos - key < 0){
          var newPos = (pos + arr.length) - key;
        }else{
          var newPos = Math.abs((pos - key) % arr.length);
        }
        text += arr[newPos];
      }else{
        text += char;
      }
  }
  return text;
}

/**
 * Creates and returns an array of emoji characters
 * if they are emojis
 * @param {string} message
 * @return arr
*/
function mergeEmojis(message){
  var arr = [];
  for(var i=0; i<message.length; i++){
    if(message.codePointAt(i) > 65535){
      arr.push(message[i] + message[i+1]);
      i += 1;
    }else{
      arr.push(message[i]);
    }
  }
  return arr;
}

/**
 * Display key in a div
 * @param {string} key
*/
function displayKey(key){
  var selectedKey = document.getElementById("selectedKey");
  while(selectedKey.firstChild){
    selectedKey.removeChild(selectedKey.firstChild);
  }
  var keyDisplayed = (en.ASCIIChars.indexOf(key) !== -1) ? key : twemoji.parse(key);
  var p = document.createElement("p");
  p.innerHTML = keyDisplayed;
  var element = p.getElementsByClassName("emoji")[0];
  if(element){
    element.className = "emojiDisplayed";
  }
  p.style.fontSize = "60px";
  selectedKey.appendChild(p);
}

/**
 * Enables input on message and attach necessary event hanlder to events
 * @param {event} e - event argument
*/
var firstCall = true;
function addEventToText(e){
  e = e || window.event;
  var target = e.target || e.srcElement;
  if(firstCall){
    if(isOldBrowser()){
      en.key = target.textContent || target.innerText;
      if(en.key.length === 1){
        displayKey(en.key);
        U.addEvent(en.inputText, "keyup", decryptMessageOld);
        en.inputText.focus();
      }else{
        en.key = undefined;
      }
    }else{
      en.key = target.alt;
      if(en.key){
        displayKey(en.key);
        U.addEvent(en.inputText, "keyup", decryptMessageEmoji);
        en.inputText.focus();
      }
    }
    if(en.key){
      en.inputText.placeholder = "Enter a message";
      en.inputText.disabled = false;
      firstCall = false;
    }
  }else{
    if(isOldBrowser()){
      en.key = target.textContent || target.innerText;
      if(en.key.length === 1){
        displayKey(en.key);
        decryptMessageOld();
      }
    }else{
      en.key = target.alt;
      if(en.key){
        displayKey(en.key);
        decryptMessageEmoji();
      }
    }
  }
}

/**
 * Initializes global variables and assign event handlers
*/
en.init = function(){
  //array [a-z]
  en.alphabetLower = charRangeArray("a","z");
  //array[A-Z]
  en.alphabetUpper = charRangeArray("A", "Z");
  //array[0-9]
  en.numeric = charRangeArray("0", "9");
  en.ASCIIChars = en.alphabetLower.concat(en.alphabetUpper).concat(en.numeric).concat(["?", "&", "!", " ", "(", ")", ".", "%", "$", "/"]);
  //regex for english characters
  en.englishRegex = /^[A-Za-z0-9?!& ().%$/]+$/;
  en.keySelect = document.getElementById("keySelect");
  en.inputText = document.getElementById("clearText");
  en.displayResult = document.getElementById("resultEncrypt");
  setDecryptionMode();
}

U.addEvent(document, "DOMContentLoaded", en.init);
